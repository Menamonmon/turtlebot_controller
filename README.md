# turtlebot_controller

### ROS 1 PACKAGE FOR CONTROLLER CODE FOR KOBUKI TURTLEBOTS

It creates the following nodes:
* `turtlebot_goal_sender_test`: an optional test node to send goals for different trajectories
* `vicon_feedback_connector`: a node that forwards the scanned `PoseStamped` object received from the appropirate `rostopic` representing the turtlebot in Vicon Tracker

## Usage:
To launch the nodes, run the `move_turtlebot` file and pass the name of the ROS node that contains the PoseStamepd received from the Vicon system as a command line argument (`vicon_pose_topic`). 

If you don't want to run the trajectory test node, make sure to comment out the corresponding line in the launch file. 