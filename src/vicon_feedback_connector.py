#!/usr/bin/env python3

# TurtleBot must have minimal.launch & amcl_demo.launch
# running prior to starting this script
# For simulation: launch gazebo world & amcl_demo prior to run this script

import rospy
from move_base_msgs.msg import MoveBaseActionFeedback, MoveBaseFeedback
from actionlib_msgs.msg import *
from geometry_msgs.msg import PoseStamped

class TFWorldToFeedbackPipeline():
	def __init__(self, bot_posestamped_topic_name, pub_queue_size=10):
		self.subscriber = rospy.Subscriber(bot_posestamped_topic_name, PoseStamped, self.subscriber_callback)
		self.publisher = rospy.Publisher("/move_base/feedback", MoveBaseActionFeedback, queue_size=pub_queue_size)
		
	def subscriber_callback(self, sub_data):
		# forward the position that we get tro move_base_msgs as a MoveBaseActionFeedback
		pose = sub_data
		feedback = MoveBaseFeedback(base_position=pose)
		action_feedback = MoveBaseActionFeedback(feedback=feedback)
		
		self.publisher.publish(action_feedback)

if __name__ == '__main__':
	rospy.init_node('vicon_feedback_connector', anonymous=False)
	vicon_pose_topic = rospy.get_param("vicon_pose_topic")
	feedback_pipeline = TFWorldToFeedbackPipeline(vicon_pose_topic)
	try:
		# # Sleep to give the last log messages time to be sent
		while True:
			try:
				pass
			except rospy.ROSInterruptException:
				break

	except rospy.ROSInterruptException:
		rospy.loginfo("Ctrl-C caught. Quitting")
